//
//  AppDelegate.swift
//  PDD-iOS
//
//  Created by Alexey Honcharov on 12.07.16.
//  Copyright © 2016 Alexey Honcharov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var rootVC:RootVC?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        rootVC = RootVC()
        rootVC!.tabBar.tintColor = UIColor.lightGray
        //tabbarRootVC.tabBar.barTintColor = UIColor.white
        
        let pdd = PddVC.storyboardInstance()
        let pddNavigator = UINavigationController(rootViewController: pdd)
        //pddNavigator.navigationBar.barTintColor = UIColor.lightGray
        pdd.tabBarItem = UITabBarItem(
            title: LocaleHelper.getLocaleString(id:LocaleHelper.pdd),
            image: #imageLiteral(resourceName: "icon_pdd"),
            tag: 1)
        
        let raznoe = RaznoeVC.storyboardInstance()
        let raznoeNavigator = UINavigationController(rootViewController: raznoe)
        //raznoeNavigator.navigationBar.barTintColor = UIColor.white
        raznoe.tabBarItem = UITabBarItem(
            title: LocaleHelper.getLocaleString(id:LocaleHelper.raznoe),
            image: #imageLiteral(resourceName: "icon_raznoe"),
            tag:2)
        
        let tests =  TestVC.storyboardInstance()
        let testsNavigator = UINavigationController(rootViewController: tests)
        //testsNavigator.navigationBar.barTintColor = UIColor.white
        tests.tabBarItem = UITabBarItem(
            title: LocaleHelper.getLocaleString(id:LocaleHelper.tests),
            image: #imageLiteral(resourceName: "icon_tests"),
            tag:3)
        
        let favorite =  FavoritesVC.storyboardInstance()
        let favoriteNavigator = UINavigationController(rootViewController: favorite)
        //favoriteNavigator.navigationBar.barTintColor = UIColor.white
        favorite.tabBarItem = UITabBarItem(
            title: LocaleHelper.getLocaleString(id:LocaleHelper.favorite),
            image: #imageLiteral(resourceName: "icon_favorites"),
            tag:4)
        
        let settings =  SettingsVC.storyboardInstance()
        let settingsNavigator = UINavigationController(rootViewController: settings)
        //settingsNavigator.navigationBar.barTintColor = UIColor.white
        settings.tabBarItem = UITabBarItem(
            title: LocaleHelper.getLocaleString(id:LocaleHelper.settings),
            image: #imageLiteral(resourceName: "icon_settings"),
            tag:5)
        
        rootVC!.viewControllers = [pddNavigator,raznoeNavigator,testsNavigator,favoriteNavigator,settingsNavigator]
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.rootViewController = rootVC
        window!.backgroundColor = UIColor.white
        window!.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

