import UIKit
import WebKit

class FavoritesVC: UIViewController,WKNavigationDelegate,WKScriptMessageHandler{
    @IBOutlet weak var contentView: UIView!
    var webView: WKWebView!
    
    static func storyboardInstance() -> FavoritesVC {
        let storyboard = UIStoryboard(name: "FavoritesVC", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "FavoritesVC") as! FavoritesVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let contentController = WKUserContentController()
        contentController.add(
            self,
            name: PddChapterVC.WEB_VIEW_CALLBACK
        )
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        self.webView = WKWebView(
            frame: CGRect.zero,
            configuration: config
        )
        self.webView.backgroundColor = UIColor.clear
        self.webView.isOpaque = false
        self.contentView.addSubview(webView)
        self.webView.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(item: webView, attribute:
            .top, relatedBy: .equal, toItem: self.contentView,
                  attribute: NSLayoutAttribute.top, multiplier: 1.0,
                  constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: webView, attribute:
            .bottom, relatedBy: .equal, toItem: self.contentView,
                     attribute: NSLayoutAttribute.bottom, multiplier: 1.0,
                     constant: 0)
        let leftConstraint = NSLayoutConstraint(item: webView, attribute:
            .leadingMargin, relatedBy: .equal, toItem: self.contentView,
                            attribute: NSLayoutAttribute.leadingMargin, multiplier: 1.0,
                            constant: 0)
        let rightConstraint = NSLayoutConstraint(item: webView, attribute:
            .trailingMargin, relatedBy: .equal, toItem: self.contentView,
                             attribute: NSLayoutAttribute.trailingMargin, multiplier: 1.0,
                             constant: 0)
        
        self.contentView.addConstraints([topConstraint, bottomConstraint, leftConstraint, rightConstraint])
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let currentFontSize = StorageService.instance.getFontSize()
        webView.evaluateJavaScript("setFontSize(\(currentFontSize))", completionHandler: nil)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
        case PddChapterVC.WEB_VIEW_CALLBACK:
            onReceivedMessage(param: message.body as! String)
            break
        default:break
        }
    }
    
    func onReceivedMessage(param:String){
        if param.contains("removeFromFavorites_"){
            let elementID = param.replacingOccurrences(of:"removeFromFavorites_", with:"").replacingOccurrences(of:".fav", with:"");
            StorageService.instance.removeFromFavorite(elementID: elementID)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = LocaleHelper.getLocaleString(id:LocaleHelper.favorite)
        if let favoritesHTML = DataHelper.getData(.favorite) as? String{
            print(favoritesHTML)
            let url = Bundle.main.bundleURL
            self.webView.navigationDelegate = self
            self.webView!.loadHTMLString(favoritesHTML, baseURL:url)
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
}
