//
//  Helper.swift
//  PDD-iOS
//
//  Created by Alexey Honcharov on 13.07.16.
//  Copyright © 2016 Alexey Honcharov. All rights reserved.
//

import Foundation

class PddChaptersVO{
    var titleJSON:JSON?
    var id = ""
}

class PddRazdelVO{
    
    var nameJSON:JSON?
    var image = ""
    var chapters = [PddChaptersVO]()
    
    static func fromJSON(_ json:JSON)->[PddRazdelVO]{
        
        var razdeli = [PddRazdelVO]()
        if let pddRazdeli = json["pddRazdeli"].array{
            
            for item in pddRazdeli{
                let razdel = PddRazdelVO()
                razdel.nameJSON = item["razdelName"]
                razdel.image = item["image"].stringValue
                if let chapters = item["chapters"].array{
                    for chapterItem in chapters{
                        let chapter = PddChaptersVO()
                        chapter.titleJSON = chapterItem["title"]
                        chapter.id = chapterItem["id"].stringValue
                        razdel.chapters.append(chapter)
                    }
                }
                
                razdeli.append(razdel)
            }
        }
        
        return razdeli
    }
}

class RaznoeVO{
    var id = ""
    var titleJSON:JSON?
    var image = ""
    
    static func fromJSON(_ json:JSON)->[RaznoeVO]{
        
        var raznoe = [RaznoeVO]()
        if let raznoeRazdeli = json["raznoe"].array{
            
            for item in raznoeRazdeli{
                let raznoeVO = RaznoeVO()
                raznoeVO.image = item["image"].stringValue
                raznoeVO.id = item["id"].stringValue
                raznoeVO.titleJSON = item["title"]
                
                raznoe.append(raznoeVO)
            }
        }
        
        return raznoe
    }
}

class TestVO{
    var id = ""
    var titleJSON:JSON?
    var image = ""
    
    static func fromJSON(_ json:JSON)->[TestVO]{
        
        var tests = [TestVO]()
        if let testsJson = json["tests"].array{
            
            for item in testsJson{
                let test = TestVO()
                test.id = item["id"].stringValue
                test.titleJSON = item["title"]
                test.image = item["image"].stringValue
                
                tests.append(test)
            }
        }
        
        return tests
    }
}

enum TabSection{
    case pddRazdel, raznoe, test, favorite, settings
}

class DataHelper{
    
    static fileprivate var themeData:JSON?
    static fileprivate var pddRazdeliData:[PddRazdelVO]!
    static fileprivate var raznoeData:[RaznoeVO]!
    static fileprivate var testsData:[TestVO]!
 
    static func getData(_ dataType:TabSection)->AnyObject?{
        
        if themeData == nil{
            if let path = Bundle.main.path(forResource: "themeData", ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: NSData.ReadingOptions.mappedIfSafe)
                    
                    themeData = JSON(data: data)
                    
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        switch dataType {
        case .pddRazdel:
            if pddRazdeliData == nil{
                pddRazdeliData = PddRazdelVO.fromJSON(DataHelper.themeData!)
            }
            return pddRazdeliData as AnyObject?
        case .raznoe:
            if raznoeData == nil{
                raznoeData = RaznoeVO.fromJSON(DataHelper.themeData!)
            }
            return raznoeData as AnyObject?
        case .test:
            if testsData == nil{
                testsData = TestVO.fromJSON(DataHelper.themeData!)
            }
            return testsData as AnyObject?
        case .favorite:
            let favorites = StorageService.instance.getFavorites()
            let favoritesHTMLPath = Bundle.main.path(forResource: "pdd_favorites", ofType: "html")
            var htmlBodyString = try! NSString(contentsOfFile:favoritesHTMLPath!, encoding:String.Encoding.utf8.rawValue) as String
            for favorite in favorites{
                htmlBodyString.append(favorite.elementHTML ?? "")
            }
            htmlBodyString.append("</body></html>")
            return htmlBodyString as AnyObject?
            
        default: return nil
        }
    }
}

public enum Locale:String{
    case ua = "ua", ru = "ru"
}

class LocaleHelper{
    static let settings = "settings"
    static let examen   = "examen"
    static let pdd      = "pdd"
    static let favorite = "favorite"
    static let choose_lang = "choose_lang"
    static let choose_text_size = "choose_text_size"
    static let lang     = "lang"
    static let rate_app = "rate_app"
    static let support  = "support"
    static let send     = "send"
    static let greeting = "greeting"
    static let tests = "tests"
    static let raznoe = "raznoe"
    
    static let lblChooseLanguage = "lblChooseLanguage"
    static let lblFontSize = "lblFontSize"
    
    static private var localeData:JSON?
    static func getLocaleString(id:String)->String?{
        let currentLocale = StorageService.instance.getLocale()
        if localeData == nil{
            if let path = Bundle.main.path(forResource: "locale", ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: NSData.ReadingOptions.mappedIfSafe)
                    
                    localeData = JSON(data: data)
                    
                } catch let error as NSError {
                    print(error.localizedDescription)
                    return nil
                }
            }
        }
        
        let localeString = localeData?[id][currentLocale].stringValue
        return localeString
    }
}
