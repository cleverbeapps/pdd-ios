import UIKit
import WebKit

class PddChapterVC: UIViewController, WKScriptMessageHandler, UITextFieldDelegate, WKNavigationDelegate{
    
    static func storyboardInstance() -> PddChapterVC {
        let storyboard = UIStoryboard(name: "PddChapterVC", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PddChapterVC") as! PddChapterVC
    }
    
    static let WEB_VIEW_CALLBACK = "webViewCallback"
    
    
    @IBOutlet var containerView : UIView! = nil
    var webView: WKWebView!
    @IBAction func btnClose(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSearchClick(_ sender: AnyObject) {
        showHideSearch();
    }
    @IBAction func btnSearchPrevClick(_ sender: AnyObject) {
        self.webView.evaluateJavaScript("scrollToPreviousScrollIndex();", completionHandler: nil);
    }
    @IBAction func btnSearchNextClick(_ sender: AnyObject) {
        self.webView.evaluateJavaScript("scrollToNextScrollIndex();", completionHandler: nil);
    }
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var btnSearchPrev: UIButton!
    @IBOutlet weak var btnSearchNext: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSearchNext.layer.shadowColor = UIColor(white: 0.0, alpha: 0.5).cgColor
        btnSearchNext.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        
        btnSearchPrev.layer.shadowColor = UIColor(white: 0.0, alpha: 0.5).cgColor
        btnSearchPrev.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        
        searchView.isHidden = true

        let contentController = WKUserContentController()
        contentController.add(
            self,
            name: PddChapterVC.WEB_VIEW_CALLBACK
        )
        
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        self.webView = WKWebView(
            frame: CGRect.zero,
            configuration: config
        )
        self.webView.backgroundColor = UIColor.clear
        self.webView.isOpaque = false
        self.containerView.addSubview(webView)
        self.webView.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(item: webView, attribute:
            .top, relatedBy: .equal, toItem: self.containerView,
                  attribute: NSLayoutAttribute.top, multiplier: 1.0,
                  constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: webView, attribute:
            .bottom, relatedBy: .equal, toItem: self.containerView,
                     attribute: NSLayoutAttribute.bottom, multiplier: 1.0,
                     constant: 0)
        let leftConstraint = NSLayoutConstraint(item: webView, attribute:
            .leadingMargin, relatedBy: .equal, toItem: self.containerView,
                            attribute: NSLayoutAttribute.leadingMargin, multiplier: 1.0,
                            constant: 0)
        let rightConstraint = NSLayoutConstraint(item: webView, attribute:
            .trailingMargin, relatedBy: .equal, toItem: self.containerView,
                             attribute: NSLayoutAttribute.trailingMargin, multiplier: 1.0,
                             constant: 0)
        
        self.containerView.addConstraints([topConstraint, bottomConstraint, leftConstraint, rightConstraint])
        
        let chapterHtmlFile = chapterID + "_" + StorageService.instance.getLocale()//"pdd_" + self.chapter.id
        let htmlPath = Bundle.main.path(forResource: chapterHtmlFile, ofType: "html")
        let html:String = try! NSString(contentsOfFile:htmlPath!, encoding:String.Encoding.utf8.rawValue) as String
        
        let url = Bundle.main.bundleURL
        self.webView.navigationDelegate = self
        self.webView!.loadHTMLString(html as String, baseURL:url)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("getChapterTitleAndFileName()", completionHandler: nil)
    }
    
    @IBAction func textFieldChanged(_ sender: UITextField) {
        if let searchText = sender.text{
            if(searchText == ""){
                self.webView.evaluateJavaScript("exitSearch();", completionHandler: nil);
            }
            else{
                findText(searchText)
            }
        }
    }
    
    func findText(_ text:String){
        self.webView.evaluateJavaScript("highlightSearchTerms('" + text + "');", completionHandler: { (o:Any?, e:Error?) in
            
            print("search "+o.debugDescription)
        })
    }
    
    func showHideSearch(){
        searchView.isHidden = !searchView.isHidden;
        if(searchView.isHidden){
            searchTF.resignFirstResponder()
            searchTF.text = ""
            self.webView.evaluateJavaScript("exitSearch();", completionHandler: nil);
        }
        else{
            searchTF.becomeFirstResponder()
        }
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
        case PddChapterVC.WEB_VIEW_CALLBACK:
            onReceivedMessage(param: message.body as! String)
            break
        default:break
        }
    }
    
    
    public var chapterID = ""
    public var currentChapterScrollToElementID = ""
    private var fileName:String = ""
    private var chapterTitle:String = ""

    func onReceivedMessage(param:String){
        var elementID:String
        var args:[String]
        
        if param.contains("isFav_"){
            let btnID:String = param.replacingOccurrences(of:"isFav_", with:"");
            elementID = btnID.replacingOccurrences(of:".fav", with:"");
            if(StorageService.instance.isFavorite(fileName:self.fileName, elementID:elementID)){
                self.webView.evaluateJavaScript("makeFavorite('" + btnID + "')", completionHandler: nil)
                //WebViewPage.callJS("makeFavorite('" + btnID + "')");
            }
            else{
                self.webView.evaluateJavaScript("unmakeFavorite('" + btnID + "')", completionHandler: nil)
                //WebViewPage.callJS("unmakeFavorite('" + btnID + "')");
            }
        }
        else if param.contains("chapterTitle_"){
            args = param.replacingOccurrences(of:"chapterTitle_", with:"").components(separatedBy: "@")  //split("@");
            self.chapterTitle = args[0]
            self.title = self.chapterTitle
            self.fileName = args[1].replacingOccurrences(of:".html", with:"")
            let currentFontSize:Int = StorageService.instance.getFontSize()
            let f = "setFontSize(\(currentFontSize),'\(currentChapterScrollToElementID)')"
            self.webView.evaluateJavaScript(f, completionHandler: nil)
            //WebViewPage.callJS("setFontSize(" + currentFontSize +"," +"'"+ CURRENT_CHAPTER_SCROLL_TO_ELEMENT_ID+"'" + ")");
            
            setTitleByChapter()
        }
        else if param.contains("addToFavorites_"){
            args = param.replacingOccurrences(of:"addToFavorites_", with:"").components(separatedBy:"@")
            let fileName:String = args[0].replacingOccurrences(of:".html", with:"");
            elementID = args[1].replacingOccurrences(of:".fav", with:"");
            let elementHTML:String = args[2];
            if StorageService.instance.isFavorite(fileName: fileName, elementID: elementID){
                let f = "unmakeFavorite('\(elementID).fav')"
                self.webView.evaluateJavaScript(f, completionHandler: nil)
                StorageService.instance.removeFromFavorite(elementID: elementID)
                //WebViewPage.callJS("unmakeFavorite('" + elementID + ".fav" + "')");
                //User.removePddFavorite(elementID);
            }
            else{
                let f = "makeFavorite('\(elementID).fav')"
                self.webView.evaluateJavaScript(f, completionHandler: nil)
                //WebViewPage.callJS("makeFavorite('" + elementID + ".fav" + "')");
                
                self.webView.evaluateJavaScript("showToast()", completionHandler: nil)
                //WebViewPage.callJS("showToast()");
                
                StorageService.instance.addToFavorite(fileName: fileName, chapterTitle:chapterTitle, elementID: elementID, elementHTML: elementHTML)
                //User.addPddFavorite(fileName, this.chapterTitle, elementID, elementHTML);
            }
            //trace(arguments);
        }
        else if param.contains("openFile_"){
            //let openFileWithID:String = param.replacingOccurrences(of:"openFile_", with:"")
            
            
            let pddChapterVC = PddChapterVC.storyboardInstance()
            pddChapterVC.chapterID = "pdd_1"//openFileWithID
            
            let navigationController = UINavigationController(rootViewController: pddChapterVC)
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    
    
    
    func setTitleByChapter(){
        
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 50))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = UIColor.black
        self.navigationItem.titleView = label
        
        label.text = chapterTitle
    }
}
