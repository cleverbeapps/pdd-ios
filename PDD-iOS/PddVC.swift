
import UIKit

enum PDDListCellCorners{
    case none, top, bottom
}

extension UIView {
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

class PDDListCell:UITableViewCell{
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var iconRight: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        iconRight.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        iconRight.isHidden = !selected
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconRight.isHidden = !isSelected
    }
}

class PddHeaderView:UITableViewHeaderFooterView{
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var icon: UIImageView!
}

class PddVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    static func storyboardInstance() -> PddVC {
        let storyboard = UIStoryboard(name: "PddVC", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PddVC") as! PddVC
    }
    
    var razdeliData = [PddRazdelVO]()
    
    @IBOutlet weak var table: UITableView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return razdeliData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return razdeliData[section].chapters.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PDDListCell", for: indexPath) as! PDDListCell;
        
        let razdel = razdeliData[(indexPath as NSIndexPath).section]
        let chapter = razdel.chapters[(indexPath as NSIndexPath).row]
        
        let currentLocale = StorageService.instance.getLocale()
        cell.label.text = chapter.titleJSON?[currentLocale].stringValue
        cell.lblNumber.text = chapter.id.replacingOccurrences(of: "pdd_", with: "")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let razdel = razdeliData[(indexPath as NSIndexPath).section]
        let chapter = razdel.chapters[(indexPath as NSIndexPath).row]
        
        let pddChapterVC = PddChapterVC.storyboardInstance()
        pddChapterVC.chapterID = chapter.id
        let navigationController = UINavigationController(rootViewController: pddChapterVC)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let currSection = razdeliData[section]
        
        let header = self.table.dequeueReusableHeaderFooterView(withIdentifier: "PddHeaderView") as! PddHeaderView
        
        let currentLocale = StorageService.instance.getLocale()
        header.label.text = currSection.nameJSON?[currentLocale].stringValue
        header.icon.image = UIImage(named:currSection.image)
        header.contentView.backgroundColor = UIColor.groupTableViewBackground
        
        return header
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.register(UINib(nibName: "PddHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "PddHeaderView")
        
        table.delegate = self
        table.dataSource = self
        table.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        razdeliData = DataHelper.getData(.pddRazdel) as! [PddRazdelVO]
        table.reloadData()
        
        title = LocaleHelper.getLocaleString(id: LocaleHelper.pdd)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
}

