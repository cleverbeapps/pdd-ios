import UIKit

class RaznoeListCell:UITableViewCell{
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var iconLeft: UIImageView!
    @IBOutlet weak var iconRight: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconRight.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        iconRight.isHidden = !selected
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconRight.isHidden = !isSelected
    }
}

class RaznoeVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    static func storyboardInstance() -> RaznoeVC {
        let storyboard = UIStoryboard(name: "RaznoeVC", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "RaznoeVC") as! RaznoeVC
    }
    
    var raznoeRazdeli = [RaznoeVO]()
    
    @IBOutlet weak var table: UITableView!
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return raznoeRazdeli.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RaznoeListCell", for: indexPath) as! RaznoeListCell
        
        let raznoeChapter = raznoeRazdeli[(indexPath as NSIndexPath).row]
        cell.iconLeft?.image = UIImage(named: raznoeChapter.image)
        
        let currentLocale = StorageService.instance.getLocale()
        cell.label.text = raznoeChapter.titleJSON?[currentLocale].stringValue
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let razdel = razdeliData[indexPath.section]
//        let chapter = razdel.chapters[indexPath.row]
//        
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let pddChapterVC = storyBoard.instantiateViewControllerWithIdentifier("PddChapterVC") as! PddChapterVC
//        pddChapterVC.chapter = chapter
//        
//        let navigationController = UINavigationController(rootViewController: pddChapterVC)
//        self.presentViewController(navigationController, animated: true, completion: nil)
        //self.tabBarController!.showViewController(navigationController, sender: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
        table.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        raznoeRazdeli = DataHelper.getData(.raznoe) as! [RaznoeVO]
        table.reloadData()
        
        title = LocaleHelper.getLocaleString(id: LocaleHelper.raznoe)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
}
