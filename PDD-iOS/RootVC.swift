//
//  RootVC.swift
//  PDD-iOS
//
//  Created by Alexey Goncharov on 26.10.16.
//  Copyright © 2016 Alexey Honcharov. All rights reserved.
//

import UIKit

class RootVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

//        let bg = UIImageView()
//        bg.contentMode = .scaleToFill
//        bg.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        bg.image = #imageLiteral(resourceName: "bg.jpg")
//        self.view.addSubview(bg)
    }
    
    func updateTitles(){
        for tabBarItem in self.tabBar.items!{
            switch tabBarItem.tag {
            case 1:
                tabBarItem.title = LocaleHelper.getLocaleString(id:LocaleHelper.pdd)
                break
            case 2:
                tabBarItem.title = LocaleHelper.getLocaleString(id:LocaleHelper.raznoe)
                break
            case 3:
                tabBarItem.title = LocaleHelper.getLocaleString(id:LocaleHelper.tests)
                break
            case 4:
                tabBarItem.title = LocaleHelper.getLocaleString(id:LocaleHelper.favorite)
                break
            case 5:
                tabBarItem.title = LocaleHelper.getLocaleString(id:LocaleHelper.settings)
                break
            default:
                break
            }
        }
    }
}
