import UIKit

class SettingsVC: UIViewController {

    static func storyboardInstance() -> SettingsVC {
        let storyboard = UIStoryboard(name: "SettingsVC", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
    }
    
    @IBOutlet weak var lblChooseLanguage: UILabel!
    @IBOutlet weak var segmentLanguage: UISegmentedControl!
    @IBOutlet weak var lblFontSize: UILabel!
    @IBOutlet weak var sliderFontSize: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderFontSize.removeTarget(self, action: #selector(SettingsVC.sliderValueDidChange(_:)), for: .valueChanged)
        sliderFontSize.value = Float(StorageService.instance.getFontSize())
        sliderFontSize.addTarget(self, action: #selector(SettingsVC.sliderValueDidChange(_:)), for: .valueChanged)
        
        segmentLanguage.removeTarget(self, action: #selector(SettingsVC.segmentValueDidChange(_:)), for: .valueChanged)
        let currentLocale = StorageService.instance.getLocale()
        segmentLanguage.selectedSegmentIndex = currentLocale == Locale.ua.rawValue ? 0 : 1
        segmentLanguage.addTarget(self, action: #selector(SettingsVC.segmentValueDidChange(_:)), for: .valueChanged)
        
        updateLabels()
    }
    
    func sliderValueDidChange(_ sender: AnyObject) {
        let step:Float = 1
        let roundedValue = round(sender.value / step) * step
        sliderFontSize.value = roundedValue
        StorageService.instance.setFontSize(size: Int(roundedValue))
    }
    
    func segmentValueDidChange(_ sender: UISegmentedControl) {
        let selectedIndex = sender.selectedSegmentIndex
        switch selectedIndex {
        case 0:
            StorageService.instance.setLocale(locale: .ua)
            break
        case 1:
            StorageService.instance.setLocale(locale: .ru)
            break
        default:break
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.rootVC?.updateTitles()
        updateLabels()
    }
    
    
    func updateLabels(){
        title = LocaleHelper.getLocaleString(id: LocaleHelper.settings)
        lblChooseLanguage.text = LocaleHelper.getLocaleString(id: LocaleHelper.lblChooseLanguage)
        lblFontSize.text = LocaleHelper.getLocaleString(id: LocaleHelper.lblFontSize)
    }
}
