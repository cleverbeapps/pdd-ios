import UIKit
import CoreData
import Realm
import RealmSwift

open class FavoriteEntity:Object{
    dynamic var elementID:String? = nil
    dynamic var fileName: String? = nil
    dynamic var chapterTitle: String? = nil
    dynamic var elementHTML: String? = nil
}
open class PreferencesEntity:Object{
    dynamic var locale:String = Locale.ua.rawValue
    dynamic var fontSize = 16
}

open class StorageService {

    public static let instance = StorageService()
    private let realm = try! Realm()
    
    public func getLocale()->String{
        return getPreferencesEntity().locale
    }
    
    public func setLocale(locale:Locale){
        let pref = getPreferencesEntity()
        try! realm.write {
            pref.locale = locale.rawValue
        }
    }
    
    public func getFontSize()->Int{
        return getPreferencesEntity().fontSize
    }
    
    public func setFontSize(size:Int){
        let pref = getPreferencesEntity()
        try! realm.write {
            pref.fontSize = size
        }
    }
    
    public func getFavorites()->Results<FavoriteEntity>{
        return realm.objects(FavoriteEntity.self)
    }
    
    public func isFavorite(fileName:String, elementID:String)->Bool{
        if let _ = getFavorites().filter("fileName = '\(fileName)' AND elementID = '\(elementID)'").first{
            return true
        }
        return false
    }
    
    public func addToFavorite(fileName:String, chapterTitle:String, elementID:String, elementHTML:String){
        let favoriteEntity = FavoriteEntity()
        favoriteEntity.fileName = fileName
        favoriteEntity.chapterTitle = chapterTitle
        favoriteEntity.elementID = elementID
        favoriteEntity.elementHTML = elementHTML
        try! realm.write {
            realm.add(favoriteEntity)
        }
    }
    
    public func removeFromFavorite(elementID:String){
        if let favoriteEntity = getFavorites().filter("elementID = '\(elementID)'").first{
            try! realm.write {
                realm.delete(favoriteEntity)
            }
        }
    }
    
    private func getPreferencesEntity()->PreferencesEntity{
        if let pref = realm.objects(PreferencesEntity.self).first{
            return pref
        }
        let pref = PreferencesEntity()
        try! realm.write {
            realm.add(pref)
        }
        return pref
    }
}
