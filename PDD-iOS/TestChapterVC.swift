import UIKit

class TestChapterVCCell:UICollectionViewCell{
    
}

class TestChapterVC: UIViewController {
    
    static func storyboardInstance() -> TestChapterVC {
        let storyboard = UIStoryboard(name: "TestChapterVC", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "TestChapterVC") as! TestChapterVC
    }
    
    @IBOutlet weak var switchontainer: GradientView!
    @IBOutlet weak var switcher: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

    }
}
