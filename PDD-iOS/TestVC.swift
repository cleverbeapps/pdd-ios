import UIKit

class TestVCCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        layer.cornerRadius = 5.0
       layer.masksToBounds = true

        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 1
        layer.shadowRadius = 7.0
        layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
    }
    
    override func prepareForReuse() {
        
    }
}

class TestVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    static func storyboardInstance() -> TestVC {
        let storyboard = UIStoryboard(name: "TestVC", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "TestVC") as! TestVC
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var tests = DataHelper.getData(.test) as! [TestVO]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.alwaysBounceVertical = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView.reloadData()
        title = LocaleHelper.getLocaleString(id:LocaleHelper.tests)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return 10.0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return tests.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestVCCell", for: indexPath) as! TestVCCell
        
        let testItem = tests[(indexPath as NSIndexPath).row]
        cell.image.image = UIImage(named: testItem.image)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/2 - 10, height: collectionView.frame.size.width/2 - 10)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
}

