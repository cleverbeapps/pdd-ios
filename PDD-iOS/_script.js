function openPddChapter(number) {
    
    var os = getMobileOperatingSystem();
    if(os == "Android"){
        window.JSInterface.openPddChapter(number);
    }
    else{
        try {
            webkit.messageHandlers.openPddChapter.postMessage(number);
        } catch(err) {
            console.log('The native context does not exist yet');
        }
    }
}
function scrollToObjectWithID(id){
    
    var el = document.getElementById(id);
    window.scrollTo(20,el.offsetTop - 70);
    
    var os = getMobileOperatingSystem();
    if(os == "Android"){
       window.JSInterface.scrollToObjectWithID(id);
       }
       else{
       try {
       webkit.messageHandlers.scrollToObjectWithID(id);
       } catch(err) {
       console.log('The native context does not exist yet');
       }
       }
}
function addToFavorites(id){
    
    var os = getMobileOperatingSystem();
    if(os == "Android"){
        window.JSInterface.addToFavorites(id);
    }
    else{
        try {
            webkit.messageHandlers.addToFavorites.postMessage(id);
        } catch(err) {
            console.log('The native context does not exist yet');
        }
    }
}
function getElementHTML(id){
    
    var element = document.getElementById(id);
    if(element != null){
        return element.outerHTML;
    }
}
function checkIsFavorite(id){
    
    var os = getMobileOperatingSystem();
    if(os == "Android"){
        window.JSInterface.checkIsFavorite(id);
    }
    else{
        try {
            webkit.messageHandlers.checkIsFavorite.postMessage(id);
        } catch(err) {
            console.log('The native context does not exist yet');
        }
    }
}
function getFontSize(){
    
    var os = getMobileOperatingSystem();
    if(os == "Android"){
        window.JSInterface.getFontSize();
    }
    else{
        try {
            webkit.messageHandlers.getFontSize.postMessage("");
        } catch(err) {
            console.log('The native context does not exist yet');
        }
    }
}
function getLocale(){
    
    var os = getMobileOperatingSystem();
    if(os == "Android"){
        window.JSInterface.getLocale();
    }
    else{
        try {
            webkit.messageHandlers.getLocale.postMessage("");
        } catch(err) {
            console.log('The native context does not exist yet');
        }
    }
}


function doHighlight(bodyText, searchTerm)
{
    currentSearchIndex = 0;
    searchTagCurrentIndex = 0;
    
    // find all occurences of the search term in the given text,
    // and add some "highlight" tags to them (we're not using a
    // regular expression search, because we want to filter out
    // matches that occur within HTML tags and script blocks, so
    // we have to do a little extra validation)
    var newText = "";
    var i = -1;
    var lcSearchTerm = searchTerm.toLowerCase();
    var lcBodyText = bodyText.toLowerCase();
    
    while (bodyText.length > 0) {
        i = lcBodyText.indexOf(lcSearchTerm, i+1);
        if (i < 0) {
            newText += bodyText;
            bodyText = "";
        } else {
            // skip anything inside an HTML tag
            if (bodyText.lastIndexOf(">", i) >= bodyText.lastIndexOf("<", i)) {
                // skip anything inside a <script> block
                if (lcBodyText.lastIndexOf("/script>", i) >= lcBodyText.lastIndexOf("<script", i)) {
                    
                    
                    var highlightStartTag = "<span class='search' id='searchIndex" + searchTagCurrentIndex + "'>";
                    var highlightEndTag = "</span>";
                    
                    newText += bodyText.substring(0, i) + highlightStartTag + bodyText.substr(i, searchTerm.length) + highlightEndTag;
                    bodyText = bodyText.substr(i + searchTerm.length);
                    lcBodyText = bodyText.toLowerCase();
                    i = -1;
                    
                    ++searchTagCurrentIndex;
                }
            }
        }
    }
    
    lastSearchTagIndex = searchTagCurrentIndex;
    
    return newText;
}

var searchTagCurrentIndex = 0;
var lastSearchTagIndex = 0;
var currentSearchIndex = 0;

function scrollToSearchIndex(index){
    currentSearchIndex = index
    scrollToObjectWithID("searchIndex"+currentSearchIndex);
}

function exitSearch(){
    reset();
}

function scrollToNextScrollIndex(){
    var next = currentSearchIndex + 1;
    if(next < lastSearchTagIndex){
        scrollToSearchIndex(next);
    }
}
function scrollToPreviousScrollIndex(){
    var prev = currentSearchIndex - 1;
    if(prev > -1){
        scrollToSearchIndex(prev);
    }
}


function makeFavorite(id){
    var favImage = document.getElementById(id);
    if(favImage != null){
        favImage.style.content = "url('fav_on.png')";
    }
}
function unmakeFavorite(id){
    var favImage = document.getElementById(id);
    if(favImage != null){
        favImage.style.content = "url('fav_off.png')";
    }
}
/*
 * INITIALIZE
 */
var initialHTML = "";
function saveInitialHTML(){
    getFontSize();
}
function setFontSize(size){
    document.body.style.fontSize = size + "pt";
    getLocale();
}
function setLocale(locale){
    
    var selector = "." + "ru";
    var myElements = document.querySelectorAll(selector);
    for (var i = 0; i < myElements.length; i++) {
        remove(myElements[i]);
    }
    
    initialHTML = document.body.innerHTML;
    
    reset();
    
    document.body.style.visibility = "visible";
    
    initTest();
}
function remove(element) {
    return (element.parentNode.removeChild(element));
}
function reset(){
    
    document.body.innerHTML = initialHTML;
    
    var myElements = document.querySelectorAll(".img-like");
    for (var i = 0; i < myElements.length; i++) {
        checkIsFavorite(myElements[i].id);
    }
}



function highlightSearchTerms(searchText, treatAsPhrase, warnOnFailure, highlightStartTag, highlightEndTag)
{
    
    reset();
    
    // if the treatAsPhrase parameter is true, then we should search for
    // the entire phrase that was entered; otherwise, we will split the
    // search string so that each word is searched for and highlighted
    // individually
    if (treatAsPhrase) {
        searchArray = [searchText];
    } else {
        searchArray = searchText.split(" ");
    }
    
    if (!document.body || typeof(document.body.innerHTML) == "undefined") {
        if (warnOnFailure) {
            alert("Sorry, for some reason the text of this page is unavailable. Searching will not work.");
        }
        return false;
    }
    
    var bodyText = document.body.innerHTML;
    for (var i = 0; i < searchArray.length; i++) {
        bodyText = doHighlight(bodyText, searchArray[i], highlightStartTag, highlightEndTag);
    }
    
    document.body.innerHTML = bodyText;
    
    scrollToSearchIndex(0);
    
    return lastSearchTagIndex;
}



/*
 * This displays a dialog box that allows a user to enter their own
 * search terms to highlight on the page, and then passes the search
 * text or phrase to the highlightSearchTerms function. All parameters
 * are optional.
 */
function searchPrompt(defaultText, treatAsPhrase, textColor, bgColor)
{
    // This function prompts the user for any words that should
    // be highlighted on this web page
    if (!defaultText) {
        defaultText = "";
    }
    
    // we can optionally use our own highlight tag values
    if ((!textColor) || (!bgColor)) {
        highlightStartTag = "";
        highlightEndTag = "";
    } else {
        highlightStartTag = "<font style='color:" + textColor + "; background-color:" + bgColor + ";'>";
        highlightEndTag = "</font>";
    }
    
    if (treatAsPhrase) {
        promptText = "Please enter the phrase you'd like to search for:";
    } else {
        promptText = "Please enter the words you'd like to search for, separated by spaces:";
    }
    
    searchText = prompt(promptText, defaultText);
    
    if (!searchText)  {
        alert("No search terms were entered. Exiting function.");
        return false;
    }
    
    return highlightSearchTerms(searchText, treatAsPhrase, true, highlightStartTag, highlightEndTag);
}


/*
 * This function takes a referer/referrer string and parses it
 * to determine if it contains any search terms. If it does, the
 * search terms are passed to the highlightSearchTerms function
 * so they can be highlighted on the current page.
 */
function highlightGoogleSearchTerms(referrer)
{
    // This function has only been very lightly tested against
    // typical Google search URLs. If you wanted the Google search
    // terms to be automatically highlighted on a page, you could
    // call the function in the onload event of your <body> tag,
    // like this:
    //   <body onload='highlightGoogleSearchTerms(document.referrer);'>
    
    //var referrer = document.referrer;
    if (!referrer) {
        return false;
    }
    
    var queryPrefix = "q=";
    var startPos = referrer.toLowerCase().indexOf(queryPrefix);
    if ((startPos < 0) || (startPos + queryPrefix.length == referrer.length)) {
        return false;
    }
    
    var endPos = referrer.indexOf("&", startPos);
    if (endPos < 0) {
        endPos = referrer.length;
    }
    
    var queryString = referrer.substring(startPos + queryPrefix.length, endPos);
    // fix the space characters
    queryString = queryString.replace(/%20/gi, " ");
    queryString = queryString.replace(/\+/gi, " ");
    // remove the quotes (if you're really creative, you could search for the
    // terms within the quotes as phrases, and everything else as single terms)
    queryString = queryString.replace(/%22/gi, "");
    queryString = queryString.replace(/\"/gi, "");
                                      
                                      return highlightSearchTerms(queryString, false);
                                      }
                                      
                                      
                                      /*
                                       * This function is just an easy way to test the highlightGoogleSearchTerms
                                       * function.
                                       */
                                      function testHighlightGoogleSearchTerms()
                                      {
                                      var referrerString = "http://www.google.com/search?q=javascript%20highlight&start=0";
                                      referrerString = prompt("Test the following referrer string:", referrerString);
                                      return highlightGoogleSearchTerms(referrerString);
                                      }



/**
 * Determine the mobile operating system.
 * This function either returns 'iOS', 'Android' or 'unknown'
 *
 * @returns {String}
 */
   function getMobileOperatingSystem() {
   var userAgent = navigator.userAgent || navigator.vendor || window.opera;
   
   if( userAgent.match( /Android/i ) )
   {
   
   return 'Android';
   }
   else
   {
   return 'iOS';
   }
   }
