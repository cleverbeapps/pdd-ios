/*
 * INITIALIZE
 */
/**
 * Determine the mobile operating system.
 * This function either returns 'iOS', 'Android' or 'unknown'
 *
 * @returns {String}
 */
function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    
    if( userAgent.match( /Android/i ) )
    {
        
        return 'Android';
    }
    else
    {
        return 'iOS';
    }
}
function webViewCallback(message) {
    
    var os = getMobileOperatingSystem();
    if(os == "Android"){
        window.JSInterface.webViewCallback(message);
    }
    else{
        try {
            webkit.messageHandlers.webViewCallback.postMessage(message);
        } catch(err) {
            console.log('The native context does not exist yet');
        }
    }
}



var initialHTML = "";
var chapterTitle = "";
var fileName = "";
var allLikeButtons = [];
function saveInitialHTML(){
    document.body.addEventListener('click', onMenuItemClick);
    this.allLikeButtons = document.querySelectorAll(".fav_off");
    this.chapterTitle = document.getElementById('chapterTitle').innerHTML;
    this.fileName = location.pathname.substring(location.pathname.lastIndexOf("/") + 1).toString();
}
function onMenuItemClick(e) {
    webViewCallback(e.target.id);
    if(e.target.className == 'fav_off' || e.target.className == 'fav_on'){
        var elementHTML = e.target.parentNode.parentNode.outerHTML.toString();

        webViewCallback('addToFavorites_' + fileName + '@' + e.target.id + '@' + elementHTML);
    }
}
function getChapterTitleAndFileName() {
    webViewCallback('chapterTitle_' + this.chapterTitle + '@' + this.fileName);
}
function setFontSize(size, scrollToElementWithID){
    document.body.style.fontSize = size + "pt";



    document.body.style.visibility = "visible";
    document.body.style.opacity = 1;

    initialHTML = document.body.innerHTML;

    for (var i = 0; i < this.allLikeButtons.length; i++) {
        checkIsFavorite(this.allLikeButtons[i].id);
    }

    if(scrollToElementWithID && scrollToElementWithID != "")
    {
        scrollToObjectWithID(scrollToElementWithID);
    }
}
function reset(){

    document.body.innerHTML = initialHTML;

    for (var i = 0; i < this.allLikeButtons.length; i++) {
        checkIsFavorite(this.allLikeButtons[i].id);
    }
}
function openFile(chapterID){
    webViewCallback("openFile_"+chapterID);
}

/*
 * FAVORITES
 */

function addToFavorites(elementID){

    var elementHTML = document.getElementById(elementID).outerHTML.toString();
   // var jsonStr = JSON.stringify(payload);
    webViewCallback('addToFavorites_' + fileName + '@' + elementID + '@' + elementHTML);
}
function getElementHTML(id){

    var element = document.getElementById(id);
    if(element != null){
        return element.outerHTML;
    }
}
function checkIsFavorite(id){
    webViewCallback('isFav_' + id);
}
function makeFavorite(id){
    var favImage = document.getElementById(id);
    if(favImage != null){
        favImage.className = 'fav_on';
    }
}
function unmakeFavorite(id){
    var favImage = document.getElementById(id);
    if(favImage != null){
        favImage.className = 'fav_off';
    }
}



/*
 * OTHER
 */

function localeChange() {
    var selectedLocale = $("[name='local']:checked").val();
    webViewCallback("selectedLocale_"+selectedLocale);
}

function openPddChapter(chapterID) {
    webViewCallback('openPddChapter_'+chapterID);
}

function showToast() {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}


/*
 * SEARCH BLOCK
 */


var searchTagCurrentIndex = 0;
var lastSearchTagIndex = 0;
var currentSearchIndex = 0;

function scrollToSearchIndex(index){
    currentSearchIndex = index;
    scrollToObjectWithID("searchIndex"+currentSearchIndex);
}
function scrollToNextScrollIndex(){
    var next = currentSearchIndex + 1;
    if(next < lastSearchTagIndex){
        scrollToSearchIndex(next);
    }
}
function scrollToPreviousScrollIndex(){
    var prev = currentSearchIndex - 1;
    if(prev > -1){
        scrollToSearchIndex(prev);
    }
}
function scrollToObjectWithID(id){
    //window.scroll(0,findPos(document.getElementById(id)));
    var e = document.getElementById(id);
    window.scrollTo(20,e.offsetTop - 250);
}
// function findPos(obj) {
//     var curtop = 0;
//     if (obj.offsetParent) {
//         do {
//             curtop += obj.offsetTop;
//         } while (obj = obj.offsetParent);
//         return [curtop];
//     }
// }
function exitSearch(){
    reset();
}
function highlightSearchTerms(searchText, treatAsPhrase, warnOnFailure, highlightStartTag, highlightEndTag)
{
    reset();

    if (treatAsPhrase) {
        searchArray = [searchText];
    } else {
        searchArray = searchText.split(" ");
    }

    if (!document.body || typeof(document.body.innerHTML) == "undefined") {
        if (warnOnFailure) {
            alert("Sorry, for some reason the text of this page is unavailable. Searching will not work.");
        }
        return false;
    }

    var bodyText = document.body.innerHTML;
    for (var i = 0; i < searchArray.length; i++) {
        bodyText = doHighlight(bodyText, searchArray[i], highlightStartTag, highlightEndTag);
    }

    document.body.innerHTML = bodyText;

    scrollToSearchIndex(0);

    return lastSearchTagIndex;
}
function doHighlight(bodyText, searchTerm)
{
    currentSearchIndex = 0;
    searchTagCurrentIndex = 0;

    // find all occurences of the search term in the given text,
    // and add some "highlight" tags to them (we're not using a
    // regular expression search, because we want to filter out
    // matches that occur within HTML tags and script blocks, so
    // we have to do a little settings validation)
    var newText = "";
    var i = -1;
    var lcSearchTerm = searchTerm.toLowerCase();
    var lcBodyText = bodyText.toLowerCase();

    while (bodyText.length > 0) {
        i = lcBodyText.indexOf(lcSearchTerm, i+1);
        if (i < 0) {
            newText += bodyText;
            bodyText = "";
        } else {
            // skip anything inside an HTML tag
            if (bodyText.lastIndexOf(">", i) >= bodyText.lastIndexOf("<", i)) {
                // skip anything inside a <script> block
                if (lcBodyText.lastIndexOf("/script>", i) >= lcBodyText.lastIndexOf("<script", i)) {


                    var highlightStartTag = "<span class='search' id='searchIndex" + searchTagCurrentIndex + "'>";
                    var highlightEndTag = "</span>";

                    newText += bodyText.substring(0, i) + highlightStartTag + bodyText.substr(i, searchTerm.length) + highlightEndTag;
                    bodyText = bodyText.substr(i + searchTerm.length);
                    lcBodyText = bodyText.toLowerCase();
                    i = -1;

                    ++searchTagCurrentIndex;
                }
            }
        }
    }

    lastSearchTagIndex = searchTagCurrentIndex;

    return newText;
}
